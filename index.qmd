---
title: "Singer, Songwriter and Performer Club"
---

## About the club

### **What does the club do?**

The club is a platform to meet musicians and organise different live/online music events - from small 'open stage', jam sessions, project-band, talks, all kinds of events to larger musical events like winter concert or music for parties or the summer fests. Since 2020, this also includes online activities. 

We do have a band room and some music/stage equipment which can be borrowed for on-site events.

### **What is it not?**

The club is not a "band", it is a platform to meet musicians. You can use it to find people to form a band, you can be a solo artist or you can team up with people for just a song for the next events.

## Communication

### **How does the club communication work?**

-   Mailinglist: [sspclub\@embl.de](mailto:sspclub@embl.de)  - all club members - feel free to use it!

-   Mailinglist: [sspclub-orga\@embl.de](mailto:sspclub-orga@embl.de) - club organisers

-   Booking calendar for EMBL Bandroom (see below)

-   [Shared Event Sheet](https://docs.google.com/spreadsheets/d/1vNwT7bnrBsN5BgFVDwXbjBLXUT4uDviwLgClnEHBYcQ/edit?usp=sharing) - list of our events 

### **Can I write "looking for musician" request to the mailing list?**

Please do so! You are very welcome.

### **Can I write "need help with X" request to the mailing list?**

Yes, you are very welcome.
